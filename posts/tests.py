from django.test import TestCase
from django.urls import reverse
from .models import Post

# Create your tests here.
class PostModelTest(TestCase): #test Models
    def setUp(self):  #create test database
        Post.objects.create(text='just a test') # a new database that has just one entry: a post with a text field containing
# the string ‘just a test’.

    def test_text_content(self): #Then we run our first test to check that the database field actually contains 'just a test'
        post=Post.objects.get(id=1) #post that represents the first id on our Post model
        expected_object_name = f'{post.text}' #let us put variables directly in our strings as long as the variables are surrounded by brackets {}
        #Here we’re setting expected_object_name to be the string of the value in post.text, which should be 'just a test'
        self.assertEqual(expected_object_name, 'just a test') #to check that our newly created entry does in fact match what we input at the top
#Test pages
class HomePageViewTest(TestCase):
    def setUp(self):
        Post.objects.create(text='this is another test')

    def test_view_url_exists_at_proper_location(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('home'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('home'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'home.html')
        
